package de.htw.ai.kbe.servlet.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import de.htw.ai.kbe.servlet.business.AuthenticationServiceImpl;
import de.htw.ai.kbe.servlet.business.IAuthenticationService;
import de.htw.ai.kbe.servlet.persistence.entities.User;
import de.htw.ai.kbe.servlet.persistence.token.ITokenPersistence;
import de.htw.ai.kbe.servlet.persistence.user.IUserPersistence;
import de.htw.ai.kbe.servlet.utils.Constants;

public class AuthResourceTest extends JerseyTest {

    @Mock
    private IUserPersistence userPersistence;
    
    @Mock
    private ITokenPersistence tokenPersistence;
    
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    
    private final static User TEST_USER;
    static {
        TEST_USER = new User();
        TEST_USER.setFirstName("Chuck");
        TEST_USER.setLastName("Tester");
        TEST_USER.setId("ctester");
    }
    
    
    @Override
    protected Application configure() {
        return new ResourceConfig(AuthResource.class).register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(AuthenticationServiceImpl.class).to(IAuthenticationService.class);
                bind(userPersistence).to(IUserPersistence.class);
                bind(tokenPersistence).to(ITokenPersistence.class);
            }
        });
    }
    
    @Test
    public void testGetWithExistingUserShouldReturnToken() throws NotFoundException {
        
        when(userPersistence.getByUserId("ctester")).thenReturn(TEST_USER);
        
        Response response = target(Constants.AUTH_RESOURCE_PATH).queryParam("userId", "ctester").request().get();
        
        assertEquals(200, response.getStatus());
        String token = response.readEntity(String.class);
        assertNotNull(token);
        System.out.println(token);      
        
        verify(userPersistence, times(1)).getByUserId("ctester");
    }

    @Test
    public void testGetWithNonExistingUserShouldReturnUnauthorized() throws NotFoundException {
        
        when(userPersistence.getByUserId("hackerman")).thenThrow(new NotFoundException());
        
        Response response = target(Constants.AUTH_RESOURCE_PATH).queryParam("userId", "hackerman").request().get();
        
        assertEquals(401, response.getStatus());
        String token = response.readEntity(String.class);
        assertTrue(token == null || token.isEmpty());
        

        verify(userPersistence, times(1)).getByUserId("hackerman");
    }

    @Test
    public void testGetWithoutUserIdShouldReturnBadRequest() throws NotFoundException {
        Response response = target(Constants.AUTH_RESOURCE_PATH).request().get();
        
        assertEquals(400, response.getStatus());
        String token = response.readEntity(String.class);
        assertTrue(token == null || token.isEmpty());
        
        verify(userPersistence, times(0)).getByUserId(anyString());
    }
    
    @Test
    public void testGetWithEmptyUserIdShouldReturnBadRequest() throws NotFoundException {
        Response response = target(Constants.AUTH_RESOURCE_PATH).queryParam("userId", "").request().get();
        
        assertEquals(400, response.getStatus());
        String token = response.readEntity(String.class);
        assertTrue(token == null || token.isEmpty());
        
        verify(userPersistence, times(0)).getByUserId(anyString());
    }
}
