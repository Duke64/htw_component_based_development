package de.htw.ai.kbe.servlet.utils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.glassfish.hk2.api.Factory;

public class InjectableEMFactory implements Factory<EntityManager>{

    private final EntityManagerFactory emf;
    
    @Inject
    public InjectableEMFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
    @Override
    public void dispose(EntityManager em) {
        em.close();
    }

    @Override
    public EntityManager provide() {
        final EntityManager em = emf.createEntityManager();
        return em;
    }

}
