package de.htw.ai.kbe.servlet.persistence.user;

import javax.ws.rs.NotFoundException;

import de.htw.ai.kbe.servlet.persistence.entities.User;

public interface IUserPersistence {

    public User getByUserId(String userId) throws NotFoundException;
}
