package de.htw.ai.kbe.servlet.persistence.token;

import de.htw.ai.kbe.servlet.persistence.entities.Token;
import de.htw.ai.kbe.servlet.persistence.entities.User;

/**
 * Interface for handling persistence operations for {@link Token}s. <br>
 * General interface for datastorage
 * @author marcel munce
 */
public interface ITokenPersistence {

    /**
     * Retrieve the token for a user
     *
     * @param user this is the user, which token you get
     * @return token
     */
    public Token findTokenByUser(User user);

    /**
     * Search for an token within the data store
     *
     * @param token token to search as String
     * @return object if found
     */
    public Token findToken(String token);

    /**
     * Saves a Tokenin the data store.
     *
     * @param token item to store
     */
    public void persistToken(Token token);

    /**
     * Updates a Token in the data store
     *
     * @param token object to update
     */
    public void updateToken(Token token);
}
