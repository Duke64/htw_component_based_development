package de.htw.ai.kbe.servlet.utils;

public final class Constants {
    private Constants() {}
    
    public static final String ROOT_RESOURCE_PATH = "/rest";
    public static final String SONGS_RESOURCE_PATH = "/songs";
    public static final String AUTH_RESOURCE_PATH = "/auth";
    public static final String SONGLIST_RESOURCE_PATH = "/userId/{userId}/songLists";
    
    public static final int TOKEN_LENGTH = 128;
    public static final String AUTH_HEADER = "Authorization";
    
    public static final String CONTEXT_USER_ID = "user_id";

    public static final String SONG_PERSISTENCE_UNIT = "song-persistence";
}
