package de.htw.ai.kbe.servlet;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

import de.htw.ai.kbe.servlet.di.DependencyBinder;
import de.htw.ai.kbe.servlet.utils.Constants;

/**
 * Entrypoint of the servlet
 *
 * @version 0.1
 */

@ApplicationPath(Constants.ROOT_RESOURCE_PATH)
public class App extends ResourceConfig {
    
    public App() {
        register(new DependencyBinder());
        packages("de.htw.ai.kbe.servlet.resources");
    }
}
