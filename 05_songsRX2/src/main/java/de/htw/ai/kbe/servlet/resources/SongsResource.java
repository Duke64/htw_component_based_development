package de.htw.ai.kbe.servlet.resources;

import static de.htw.ai.kbe.servlet.utils.Utils.buildErrorResponse;

import de.htw.ai.kbe.servlet.persistence.entities.Song;
import de.htw.ai.kbe.servlet.persistence.song.ISongPersistence;
import de.htw.ai.kbe.servlet.utils.Constants;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Servlet to read and write Songs into a simple kind of database
 *
 * @version 0.1
 */
@Path(Constants.SONGS_RESOURCE_PATH)
public class SongsResource {

    private static final Logger LOG = Logger.getLogger(SongsResource.class);

    @Context
    UriInfo uriInfo;

    @Inject
    private ISongPersistence songsPersistence;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Song> getAllSongs() {
        LOG.info("Get all songs");
        return songsPersistence.getAll();
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getSong(@PathParam("id") Integer id) {
        LOG.info("Get song by id (" + id + ")");

        try {
            Song song = songsPersistence.getById(id);
            return Response.ok(song).build();
        } catch (NoSuchElementException e) {
            return buildErrorResponse(Status.NOT_FOUND, "No song with id " + id);
        }
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces(MediaType.TEXT_PLAIN)
    public Response createSong(@Valid Song song) {
        LOG.info("Creating new song");
        try {
            Integer newId = songsPersistence.add(song);
            UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
            uriBuilder.path(newId.toString());
            return Response.created(uriBuilder.build()).entity("Song added (new id: " + newId + ")").build();
        } catch (PersistenceException e) {
            return buildErrorResponse(Status.BAD_REQUEST, "Could not add the song. Maybe wrong payload. " + e.getMessage());
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}")
    public Response updateSong(@PathParam("id") Integer id, @Valid Song song) {
        LOG.info("Updating song (id " + id + ")");

        if (song.getId() != null && id != song.getId()) {
            return buildErrorResponse(Status.BAD_REQUEST, "Id missmatch between argument and payload");
        }

        song.setId(id);
        try {
            songsPersistence.update(song);
        } catch (NoSuchElementException e) {
            return buildErrorResponse(Status.NOT_FOUND, "Song not found");
        }
        return Response.status(Status.NO_CONTENT).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Integer id) {
        LOG.info("Deleting song (id " + id + ")");

        try {
            songsPersistence.delete(id);
            return Response.status(Status.NO_CONTENT).build();
        } catch (NoSuchElementException e) {
            return buildErrorResponse(Status.NOT_FOUND, "No song with id " + id);
        } catch (PersistenceException e) {
            return buildErrorResponse(Status.BAD_REQUEST, "Cannot delete song. It is in use or linked to other entries/tables. " + e.getMessage());
        }
    }
}
