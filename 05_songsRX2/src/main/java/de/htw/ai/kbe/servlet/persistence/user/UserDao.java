package de.htw.ai.kbe.servlet.persistence.user;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.NotFoundException;

import de.htw.ai.kbe.servlet.persistence.entities.User;

public class UserDao implements IUserPersistence {

    @Inject
    private EntityManager em;
    
    @Override
    public User getByUserId(String userId) throws NotFoundException {
        User u = em.find(User.class, userId);
        if(u == null) {
            throw new NotFoundException("No such user '" + userId + "'");
        }
        return u;
    }

}
