package de.htw.ai.kbe.servlet.persistence.token;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import de.htw.ai.kbe.servlet.persistence.entities.Token;
import de.htw.ai.kbe.servlet.persistence.entities.User;

public class TokenDao implements ITokenPersistence {

    @Inject
    private EntityManager em;

    @Override
    public void persistToken(Token token) {
        em.getTransaction().begin();
        em.persist(token);
        em.getTransaction().commit();
    }

    @Override
    public Token findToken(String token) {
        Query q = em.createQuery("SELECT t FROM Token t WHERE t.token = :token");
        q.setParameter("token", token);
        try {
            return (Token) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Token findTokenByUser(User user) {
        Query q = em.createQuery("SELECT t FROM Token t WHERE t.user = :user");
        q.setParameter("user", user);
        try {
            return (Token) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public void updateToken(Token token) {
        em.getTransaction().begin();
        em.merge(token);
        em.getTransaction().commit();
    }

}
