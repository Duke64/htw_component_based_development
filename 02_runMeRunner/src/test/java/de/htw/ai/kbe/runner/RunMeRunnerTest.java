package de.htw.ai.kbe.runner;

import de.htw.ai.kbe.runner.report.RunMeReport;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test cases for the {@link RunMeRunner} class
 */
public class RunMeRunnerTest {

    private static final String CLASS_TO_TEST = "de.htw.ai.kbe.runner.TestClass";

    /**
     * Test for a successful execution of "RunMeRunner.execute" and reflection of the "TestClass"
     */
    @Test
    public void testSuccessfulExecution() {
        RunMeReport report = null;
        try {
            report = RunMeRunner.execute(CLASS_TO_TEST);
        } catch (Exception e) {
            fail("Exception should not have been thrown");
        }
        assertNotNull(report);

        // Total numbers of methods
        assertEquals(5, report.getMethodCount());

        // Numbers of methods with @RunMe Annotation
        assertEquals(3, report.getMethodNamesWithRunMe().size());

        // Numbers of not invokable methods with @RunMe Annotation
        assertEquals(2, report.getMethodNamesWithNotInvokable().size());

        // The names of the not invokable methods
        assertThat(report.getMethodNamesWithNotInvokable(), CoreMatchers.hasItems("annotationMethodWithParam", "notInvokableAnnotationMethod"));

    }

    /**
     * Test case for an empty/null class name path
     *
     * @throws RunMeRunnerException
     */
    @Test(expected = RunMeRunnerException.class)
    public void testClassNameIsEmpty() throws RunMeRunnerException {
        RunMeReport report = RunMeRunner.execute(null);
        assertNull(report);

        report = RunMeRunner.execute("");
        assertNull(report);
    }

    /**
     * Test case for an instance class with an constructor
     *
     * @throws RunMeRunnerException
     */
    @Test(expected = RunMeRunnerException.class)
    public void testClassWithConstructor() throws RunMeRunnerException {
        RunMeReport report = RunMeRunner.execute("de.htw.ai.kbe.runner.TestClassWithConstructors");
        assertNull(report);
    }
}