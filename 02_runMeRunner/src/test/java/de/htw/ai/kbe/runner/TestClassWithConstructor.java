package de.htw.ai.kbe.runner;

import de.htw.ai.kbe.runner.annotations.RunMe;

/**
 * TestClass for the {@link de.htw.ai.kbe.runner.RunMeRunner} tests.<br>
 * With Constructor
 */
public class TestClassWithConstructor {

    public TestClassWithConstructor(String a) {
    }

    @RunMe
    public void foo() {
    }
}
