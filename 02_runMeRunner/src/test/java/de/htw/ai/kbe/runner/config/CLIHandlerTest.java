package de.htw.ai.kbe.runner.config;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * JUnit test for {@link CLIHandler}
 */
public class CLIHandlerTest {

    private final static String PROPERTIES_CLI_VALUE = "test.properties";
    private final static String REPORT_CLI_VALUE = "test_report.txt";


    @Test(expected = ConfigException.class)
    public void testInitNoArguments() throws ConfigException {
        IConfigProvider provider = CLIHandler.init(null);
        assertNull(provider);
    }

    @Test(expected = ConfigException.class)
    public void testMissingArguments() throws ConfigException {
        IConfigProvider provider = CLIHandler.init(new String[]{
                "-p", "test.properties"
        });
        assertNull(provider);
    }

    @Test(expected = ConfigException.class)
    public void testUnknownArguments() throws ConfigException {
        IConfigProvider provider = CLIHandler.init(new String[]{
                "-xy", "bla bla "
        });
        assertNull(provider);
    }

    @Test
    public void testSuccessfulGetConfig() {
        IConfigProvider provider = null;
        try {
            provider = CLIHandler.init(new String[]{
                    "-p", PROPERTIES_CLI_VALUE,
                    "-o", REPORT_CLI_VALUE
            });
        } catch (ConfigException e) {
            fail("Exception should not been have thrown: " + e.getMessage());
        }
        assertNotNull(provider);

        IConfig config = null;
        try {
            config = provider.getConfig();
        } catch (ConfigException e) {
            fail("Exception should not been have thrown: " + e.getMessage());
        }
        assertNotNull(config);

        assertEquals(PROPERTIES_CLI_VALUE, config.getPropertiesFile());
        assertEquals(REPORT_CLI_VALUE, config.getOutputFile());
    }
}
