package de.htw.ai.kbe.runner.report;

/**
 * Exception for {@link IReportWriter}
 */
public class ReportWriterException extends Exception {

    /**
     * Exception when something goes wrong for writing an report
     */
    private static final long serialVersionUID = 1L;

    public ReportWriterException(String message) {
        super(message);
    }
}
