package de.htw.ai.kbe.runner;

/**
 * Exception for {@link RunMeRunner} class
 */
public class RunMeRunnerException extends Exception {

	private static final long serialVersionUID = 1L;

	public RunMeRunnerException(String msg) {
        super(msg);
    }
}
