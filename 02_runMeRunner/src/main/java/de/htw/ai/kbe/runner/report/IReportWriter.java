package de.htw.ai.kbe.runner.report;

/**
 * Interface for writing reports
 */
public interface IReportWriter {

    /**
     * Write an {@link RunMeReport}
     *
     * @param report to write
     * @throws ReportWriterException
     */
    void writeReport(RunMeReport report) throws ReportWriterException;
}