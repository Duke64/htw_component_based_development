package de.htw.ai.kbe.runner.config;

/**
 * Interface for program's configuration parameters
 */
public interface IConfig {

    /**
     * @return properties file path
     */
    String getPropertiesFile();

    /**
     * @return path of report output file
     */
    String getOutputFile();
}
