package de.htw.ai.kbe.runner.config;

import org.apache.commons.cli.*;

/**
 * {@link IConfigProvider} implementation parsing the command line
 * arguments.<br>
 * An instance is obtained using the {@link #init(String[])} method
 */
public class CLIHandler implements IConfigProvider {

    private static final String USAGE = "java -jar runMeRunner-1.0-jar-with-dependencies.jar";
    private HelpFormatter helpFormatter;
    private Options options;
    private CommandLine cli;

    /**
     * Default Constructor<br>
     * Initializes the formatter and cli-options
     */
    private CLIHandler() {
        this.helpFormatter = new HelpFormatter();
        this.options = initOptions();
    }

    /**
     * Factory method for obtaining an {@link IConfigProvider} instance.<br>
     *
     * @param args command line arguments
     * @return instance of {@link IConfigProvider}
     * @throws ConfigException in case arguments were invalid
     */
    public static IConfigProvider init(String[] args) throws ConfigException {
        CLIHandler handler = new CLIHandler();
        handler.initOptions();
        handler.initCommandLine(args);
        return handler;
    }

    /**
     * Initializing the cli-options<br>
     * Here: "p" and "o"
     *
     * @return instance of {@link Options}
     */
    private Options initOptions() {
        Options options = new Options();

        Option input = Option.builder("p").desc("Configuration input file. Properties with class to run.").hasArg()
                .required().argName("properties").build();

        Option report = Option.builder("o").desc("Report output file.").hasArg().required().argName("report").build();

        options.addOption(input);
        options.addOption(report);

        return options;
    }

    /**
     * Initializing the cli and handles the user arguments
     *
     * @param args the arguments given by a user
     * @throws ConfigException in case arguments were invalid
     */
    private void initCommandLine(String[] args) throws ConfigException {
        try {
            CommandLineParser parser = new DefaultParser();
            this.cli = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            helpFormatter.printHelp(USAGE, options, true);
            throw new ConfigException("Failed to parse arguments. " + e.getMessage());
        }
    }

    /**
     * Get the program's configuration parameters
     *
     * @return instance of {@link IConfig}
     */
    @Override
    public IConfig getConfig() {
        String propertiesFile = cli.getOptionValue("p");
        String outputFile = cli.getOptionValue("o");

        return new IConfig() {

            @Override
            public String getPropertiesFile() {
                return propertiesFile;
            }

            @Override
            public String getOutputFile() {
                return outputFile;
            }
        };
    }
}
