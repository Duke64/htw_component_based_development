package de.htw.ai.kbe.runner.config;

/**
 * Exception when something goes wrong during the handling of the configuration or arguments
 */
public class ConfigException extends Exception {

    private static final long serialVersionUID = 1L;

    public ConfigException(String message) {
        super(message);
    }
}
