package de.htw.ai.kbe.runner;

import de.htw.ai.kbe.runner.annotations.RunMe;

/**
 * TestClass for the {@link de.htw.ai.kbe.runner.RunMeRunner} tests
 */
public class TestClass {

    @RunMe
    public void invokableAnnotationMethod() {
    }

    @RunMe
    private void notInvokableAnnotationMethod() {
    }

    @RunMe
    public void annotationMethodWithParam(int a) {
    }

    public void invokableMethod() {
    }

    protected void notInvokableMethod() {
    }
}