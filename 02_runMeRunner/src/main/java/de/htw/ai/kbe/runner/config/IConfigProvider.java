package de.htw.ai.kbe.runner.config;

/**
 * Interface for program's configuration
 */
public interface IConfigProvider {

    /**
     * @return config
     */
    IConfig getConfig() throws ConfigException;
}