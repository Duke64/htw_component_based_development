package de.htw.ai.kbe.servlet.marshalling.impl;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import de.htw.ai.kbe.servlet.marshalling.IMarshaller;
import de.htw.ai.kbe.servlet.marshalling.MarshallingException;
import de.htw.ai.kbe.servlet.pojo.Song;
import de.htw.ai.kbe.servlet.pojo.Songs;

/**
 * Can read and write XML files for one or more {@link Song}
 * 
 * @version 0.5
 */
class XmlHandler implements IMarshaller {

    private final static Logger LOG = Logger.getLogger(XmlHandler.class);

    // Package wide
    XmlHandler() {
    }
    

    @Override
    public Song readSongFromStream(InputStream is) throws MarshallingException {
//        return readFromStream(is, Song.class);
        /**
         * When a single Song is posted, it is still wrapped in <songs></songs>
         * Therefore handle as list and return the first element
         */
       
        List<Song> list = readSongsFromStream(is);
        if(list.size() != 1) {
            LOG.warn("Parsing of Songs worked, but was no single song");
            throw new MarshallingException("Not a single song");
        }
        return list.get(0);
        
    }
    
    @Override
    public List<Song> readSongsFromStream(InputStream is) throws MarshallingException {
        Songs songs = readFromStream(is, Songs.class);
        return songs.getSongList();
    }
    
    @SuppressWarnings("unchecked")
    private <T> T readFromStream(InputStream is, Class<T> clazz) throws MarshallingException {
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            T songs = (T) unmarshaller.unmarshal(is);
            return songs;
        } catch (Exception e) {
            String msg = "Failed to read Song(s) from stream. " + e.getMessage();
            LOG.warn(msg);
            throw new MarshallingException(msg);
        }
    }

    @Override
    public void writeSongsToStream(List<Song> songs, OutputStream os) throws MarshallingException {
        Songs songsWrapper = new Songs();
        songsWrapper.setSongList(songs);

        try {
            JAXBContext context = JAXBContext.newInstance(Songs.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(songsWrapper, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            String msg = "Failed to write Songs to stream. " + e.getMessage();
            LOG.warn(msg);
            throw new MarshallingException(msg);
        }
    }
}
