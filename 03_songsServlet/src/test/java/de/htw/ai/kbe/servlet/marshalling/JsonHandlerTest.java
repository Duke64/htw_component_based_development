package de.htw.ai.kbe.servlet.marshalling;

import static de.htw.ai.kbe.servlet.utils.TestUtils.assertSong;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.htw.ai.kbe.servlet.marshalling.impl.MarshallerFactory;
import de.htw.ai.kbe.servlet.pojo.Song;
import de.htw.ai.kbe.servlet.utils.Constants;
import de.htw.ai.kbe.servlet.utils.TestUtils;

/**
 * Test cases for the {@link JsonHandler} class
 *
 * @Version 0.5
 */
public class JsonHandlerTest {

    private static String FILENAME_READINGJSON = "testReadSongs.json";
    private static String FILENAME_READINGSINGLEJSON = "testReadSingleSong.json";
    private static String FILENAME_WRITINGJSON = "testWriteSongs.json";

    private String filePathForReading;
    private String filePathForWriting;
    private IMarshaller handler;
    private String filePathForSingleReading;

    @Before
    public void setUp() {
        filePathForReading = getClass().getResource(FILENAME_READINGJSON).getFile();
        filePathForSingleReading = getClass().getResource(FILENAME_READINGSINGLEJSON).getFile();
        filePathForWriting = getClass().getResource(FILENAME_READINGJSON).getFile().replace(FILENAME_READINGJSON, FILENAME_WRITINGJSON);
        handler = MarshallerFactory.getInstance().getMarshaller(Constants.CONTENTTYPE_JSON);
    }

    /**
     * Test for a successful reading of a song.json file into objects
     */
    @Test
    public void testReadJsonFileSuccessful() {
        try(InputStream is = new BufferedInputStream(new FileInputStream(filePathForReading))) {
            List<Song> testSong = handler.readSongsFromStream(is);

            assertEquals(2, testSong.size());

            assertSong(testSong.get(0), 6, "Pantera", "Cowboys From Hell", "Domination", 1990);
            assertSong(testSong.get(1), 1, "Ufomammut", "Idolum", "Stigma", 2008);

        } catch (Exception e) {
            System.err.println(e.getMessage());
            fail("Exception during method readSongsFromStream");
        }
    }
    
    @Test
    public void testReadJsonFileWithSingleSong() {
        try(InputStream is = new BufferedInputStream(new FileInputStream(filePathForSingleReading))) {
            Song testSong = handler.readSongFromStream(is);


            assertSong(testSong, 1, "Sylosis", "Conclusion Of An Age", "After Lifeless Years", 2008);

        } catch (Exception e) {
            System.err.println(e.getMessage());
            fail("Exception during method readSongFromStream");
        }
    }

    /**
     * Test for a successful writing of a test xml
     * It only checks the creation and filling.
     * It checks not the correct filling with the correct content, cause we don't use the reading method.
     * We avoid to test two different methods in one testcase. You have to check the content manually.
     */
    @Test
    public void testWriteJsonFileSuccessful() {
        List<Song> songList = TestUtils.getTestSongs();

        try(OutputStream os = new BufferedOutputStream(new FileOutputStream(filePathForWriting))) {
            handler.writeSongsToStream(songList, os);
        } catch (Exception e) {
            fail("Exception during method writeSongsToStream");
        }

        // check if written json file is empty
        try(BufferedReader br = new BufferedReader(new FileReader(filePathForWriting))){
            if (br.readLine() == null) {
                fail("JSON file is empty");
            }
        } catch (Exception e) {
            fail("Exception during test if written json file is empty");
        }
    }
}