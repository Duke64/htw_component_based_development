package de.htw.ai.kbe.properties;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * ABC
 *
 * @author Tom Dittrich s0555944@htw-berlin.de
 * @version 0.5
 * @date 18.04.2018
 */
public class PropsFileUtil {

    public static Properties readPropsFile(String filename) throws PropsFileReadException{
        if(filename == null) {
            throw new PropsFileReadException("filename is null");
        }
        try(BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filename))) {
            Properties props = new Properties();
            props.load(bis);
            return props;
        }  catch (FileNotFoundException e) {
            throw new PropsFileReadException("File '" + filename + "' not found");
        } catch (IOException e) {
            throw new PropsFileReadException("IO error occurred (" + e.getMessage() +")");
        }
    }
}
