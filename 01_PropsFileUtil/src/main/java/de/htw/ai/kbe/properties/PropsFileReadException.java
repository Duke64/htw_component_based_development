package de.htw.ai.kbe.properties;

/**
 * Exception for property file read
 *
 * @author Tom Dittrich s0555944@htw-berlin.de
 * @version 0.5
 * @date 18.04.2018
 */
public class PropsFileReadException extends Exception {

    public PropsFileReadException(String exceptionMessage){
        super(exceptionMessage);
    }
}
