package de.htw.ai.kbe.properties;

import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.*;

public class PropsFileUtilTest {

    @Test
    public void testReadPropsFileSuccessful() {
        try {
            String file = getClass().getResource("test.properties").getFile();
            Properties properties = PropsFileUtil.readPropsFile(file);
            assertNotNull(properties);
            assertEquals(1, properties.size());
            assertEquals("testvalue", properties.getProperty("test.key"));
        } catch (PropsFileReadException e) {
            fail("Exception should not have been thrown");
        }
    }

    @Test(expected = PropsFileReadException.class)
    public void testReadPropsFileNullFile() throws PropsFileReadException {
        PropsFileUtil.readPropsFile(null);
    }

    @Test(expected = PropsFileReadException.class)
    public void testReadPropsFileNotFound() throws PropsFileReadException {
        PropsFileUtil.readPropsFile("gibbet_nicht.properties");
    }

    @Test(expected = PropsFileReadException.class)
    public void testReadPropsFileNoProperties() throws PropsFileReadException {
        PropsFileUtil.readPropsFile("test.txt");
    }
}