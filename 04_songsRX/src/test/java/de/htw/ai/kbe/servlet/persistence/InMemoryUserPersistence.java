package de.htw.ai.kbe.servlet.persistence;

import java.util.HashMap;
import java.util.Map;

import de.htw.ai.kbe.servlet.persistence.user.IUserPersistence;
import de.htw.ai.kbe.servlet.pojo.User;
import javassist.NotFoundException;

public class InMemoryUserPersistence implements IUserPersistence {

    private Map<String, User> users;
    
    public InMemoryUserPersistence() {
        users = new HashMap<>();
        fillTestUsers();
    }
    
    private void fillTestUsers() {
        User u1 = new User();
        u1.setId(1);
        u1.setFirstName("Chuck");
        u1.setLastName("Tester");
        u1.setUserId("ctester");
        users.put(u1.getUserId(), u1);
    }

    @Override
    public User getByUserId(String userId) throws NotFoundException {
        User u = users.get(userId);
        if(u == null) throw new NotFoundException("user not found " + userId);
        return u;
    }

}
