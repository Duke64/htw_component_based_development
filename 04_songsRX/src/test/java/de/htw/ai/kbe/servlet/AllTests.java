package de.htw.ai.kbe.servlet;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.htw.ai.kbe.servlet.persistence.song.InMemorySongsPersistenceTest;
import de.htw.ai.kbe.servlet.persistence.user.JSONUserPersistenceTest;
import de.htw.ai.kbe.servlet.resources.AuthResourceTest;
import de.htw.ai.kbe.servlet.resources.SongsResourceTest;

@RunWith(Suite.class)
@SuiteClasses({ 
    JSONUserPersistenceTest.class,
    SongsResourceTest.class,
    InMemorySongsPersistenceTest.class,
    AuthResourceTest.class
    })
public class AllTests {

}
