package de.htw.ai.kbe.servlet.persistence.song;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.htw.ai.kbe.servlet.pojo.Song;
import org.apache.log4j.Logger;

import javax.ws.rs.NotFoundException;
import java.io.InputStream;
import java.util.*;

/**
 * Implementation of {@link ISongPersistence} for {@link Song} objects.<br>
 * Songs were stored in a synchronized HashMap
 *
 * @author marcel munce
 */
public class InMemorySongsPersistence implements ISongPersistence {

    private static final Logger LOG = Logger.getLogger(InMemorySongsPersistence.class);

    private int idCounter;
    private Map<Integer, Song> songs;

    public InMemorySongsPersistence() {
        this(getDefaultSongs());
    }

    InMemorySongsPersistence(List<Song> defaultSongs) {
        LOG.info("Constructing InMemorySongsPersistance");
        songs = Collections.synchronizedMap(new HashMap<>());
        if (defaultSongs == null) {
            defaultSongs = new ArrayList<>();
        }
        defaultSongs.stream().filter(s -> s.getId() != null)
                .sorted((a, b) -> a.getId().compareTo(b.getId()))
                .forEach(s -> songs.put(s.getId(), s));
        // Order defaultSongs by id and take maxId + 1 as counterstart. Otherwise start at 0
        idCounter = defaultSongs.stream().filter(s -> s.getId() != null).map(Song::getId).max(Comparator.naturalOrder()).orElse(-1) + 1;
    }

    @Override
    public synchronized List<Song> getAll() {
        LOG.info("Get all songs");
        return new ArrayList<>(songs.values());
    }

    @Override
    public synchronized Song getById(int id) throws NotFoundException {
        LOG.info("Get song by id '" + id + "'");
        Song song = songs.get(id);
        if (song == null) {
            LOG.debug("No song found");
            throw new NotFoundException("No song with id '" + id + "'!");
        }
        return song;
    }

    @Override
    public synchronized void delete(int id) throws NotFoundException {
        LOG.info("Deleting song with id '" + id + "'");
        if (songs.remove(id) == null) {
            LOG.debug("Song not found");
            throw new NotFoundException("No song with id '" + id + "'!");
        }
    }

    @Override
    public synchronized int add(Song item) {
        LOG.info("Adding new song");
        item.setId(idCounter++);
        songs.put(item.getId(), item);
        return item.getId();
    }

    @Override
    public synchronized void update(Song song) throws NotFoundException {
        LOG.info("Updating song with id '" + song.getId() + "'");
        if (song.getId() == null || !songs.containsKey(song.getId())) {
            LOG.debug("Song not found");
            throw new NotFoundException("No song with id '" + song.getId() + "'!");
        }
        songs.put(song.getId(), song);
    }

    static List<Song> getDefaultSongs() {
        List<Song> songs = new ArrayList<>();

        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream is = InMemorySongsPersistence.class.getResourceAsStream("songs.json")) {
            songs = objectMapper.readValue(is, new TypeReference<List<Song>>() {
            });
        } catch (Exception e) {
            // in case of missing file, there are three songs
            Song song = new Song();
            song.setId(1);
            song.setArtist("Pantera");
            song.setAlbum("Cowboys From Hell");
            song.setReleased(1990);
            song.setTitle("Domination");
            songs.add(song);

            song = new Song();
            song.setId(2);
            song.setTitle("After Lifeless Years");
            song.setArtist("Sylosis");
            song.setAlbum("Conclusion Of An Age");
            song.setReleased(2008);
            songs.add(song);

            song = new Song();
            song.setId(3);
            song.setTitle("Stigma");
            song.setArtist("Ufomammut");
            song.setAlbum("Idolum");
            song.setReleased(2008);
            songs.add(song);
        }
        return songs;
    }

}