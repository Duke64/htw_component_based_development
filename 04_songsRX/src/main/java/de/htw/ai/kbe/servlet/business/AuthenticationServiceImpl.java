package de.htw.ai.kbe.servlet.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;

import org.apache.log4j.Logger;

import de.htw.ai.kbe.servlet.persistence.user.IUserPersistence;
import de.htw.ai.kbe.servlet.pojo.User;
import javassist.NotFoundException;

public class AuthenticationServiceImpl implements IAuthenticationService {

    private static final Logger LOG = Logger.getLogger(AuthenticationServiceImpl.class);

    @Inject
    private IUserPersistence userPersistence;

    private Map<String, String> userTokens;

    public AuthenticationServiceImpl() {
        userTokens = Collections.synchronizedMap(new HashMap<>());
    }

    @Override
    public synchronized String authenticate(String userId) throws NotAuthorizedException {
        LOG.info("Authenticating user " + userId);
        User user;
        try {
            user = userPersistence.getByUserId(userId);
        } catch (NotFoundException e) {
            throw new NotAuthorizedException("User does not exist");
        }

        String token = generateToken(user);

        
        userTokens.put(userId, token);

        return token;
    }

    private String generateToken(User user) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest((user.getUserId() + UUID.randomUUID().toString()).getBytes(StandardCharsets.UTF_8));

            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            // In case SHA-256 is not found, something's really really wrong.
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isValid(String token) {
        return userTokens.containsValue(token);
    }
}