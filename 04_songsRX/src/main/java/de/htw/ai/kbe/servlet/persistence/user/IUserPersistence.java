package de.htw.ai.kbe.servlet.persistence.user;

import de.htw.ai.kbe.servlet.pojo.User;
import javassist.NotFoundException;

public interface IUserPersistence {

    public User getByUserId(String userId) throws NotFoundException;
}
