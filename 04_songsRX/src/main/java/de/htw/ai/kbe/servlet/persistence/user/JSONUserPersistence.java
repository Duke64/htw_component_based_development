package de.htw.ai.kbe.servlet.persistence.user;

import java.io.File;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.htw.ai.kbe.servlet.pojo.User;
import javassist.NotFoundException;

public class JSONUserPersistence implements IUserPersistence {
    
    private static final Logger LOG = Logger.getLogger(JSONUserPersistence.class);
    private static JSONUserPersistence instance = null;
    
    private ObjectMapper mapper;
    private File jsonFile;
    
    public static JSONUserPersistence getInstance() {
        if(instance==null) {
            instance = new JSONUserPersistence(new File(JSONUserPersistence.class.getResource("users.json").getFile()));
        }
        return instance;
    }
    
    JSONUserPersistence(File file) {
        mapper = new ObjectMapper();
        jsonFile = file;
    }

    @Override
    public User getByUserId(String userId) throws NotFoundException {
        LOG.info("Get user for userId: " + userId);
        List<User> users = getAllUsers();
        
        Optional<User> user = users.stream().filter(u -> u.getUserId().equals(userId)).findFirst();
        if(user.isPresent()) {
            LOG.debug("Found user");
            return user.get();
        }
        LOG.info("No user found");
        throw new NotFoundException("No user with userid '" + userId + "'.");
    }
    
    protected List<User> getAllUsers() {
        LOG.info("Getting all users");
        try {
            List<User> users = mapper.readValue(jsonFile, mapper.getTypeFactory().constructCollectionType(List.class, User.class));
            return users;
        } catch (Exception e) {
            LOG.fatal("Failed to read user file.", e);
            throw new RuntimeException("Failed to read user json file");
        }
    }

}
