package de.htw.ai.kbe.servlet.utils;

public final class Constants {
    private Constants() {}
    
    public static final String ROOT_RESOURCE_PATH = "/rest";
    public static final String SONGS_RESOURCE_PATH = "/songs";
    public static final String AUTH_RESOURCE_PATH = "/auth";
    
    public static final int TOKEN_LENGTH = 128;
    public static final String AUTH_HEADER = "Authorization";
}
