package de.htw.ai.kbe.servlet.persistence.song;

import java.util.List;
import java.util.NoSuchElementException;

import de.htw.ai.kbe.servlet.pojo.Song;

/**
 * Interface for handling persistence operations for {@link Song}s. <br>
 * General interface for datastorage
 * @author marcel munce
 *
 */
public interface ISongPersistence {

    /**
     * @return List of all songs saved 
     */
    List<Song> getAll();
    
    /**
     * Retrieve a {@link Song} by specific id
     * @param id id of Song
     * @return object if found
     * @throws NoSuchElementException in case no object with given Id exists
     */
    Song getById(int id) throws NoSuchElementException;
    
    /**
     * Removes a {@link Song} from the data store by the id
     * @param id id of object to delete
     * @throws NoSuchElementException in case no object with given Id exists
     */
    void delete(int id) throws NoSuchElementException;
    
    /**
     * Saves a {@link Song} in the data store.<br>
     * This operation sets the object's id field!
     * @param item item to store
     * @return primary identifier of added item
     */
    int add(Song item);
    
    /**
     * Updates a {@link Song} in the data store using the id.
     * @param item object to update
     * @throws NoSuchElementException in case no object with given Id exists
     */
    void update(Song item) throws NoSuchElementException;
}
